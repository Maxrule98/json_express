const express = require("express");
const fs = require('fs');
const app = express();

const PORT = process.env.PORT || 3000;
app.use(express.static('public'));

//requiring data file
let data = require('./text.json');



app.get('/data', (req, res, next) => {
    res.send(data);
});

app.get('/data/topic', (req, res, next) => {
    res.send(data.topic);
});

app.get('/data/people', (req, res, next) => {
    res.send(data.people);
});

/*
app.get('/data/people/0', (req, res, next) => {
    res.send(data.people[0]);
});
*/

app.get('/data/people/:id', (req, res, next) => {
    const getId = req.params.id;
    const id = data.people[getId];
    if (id) {
        res.send(id);
    } else {
        res.status(404).send('Incorrect ID');
    }
});

app.get('/data/people/:id/firstname', (req, res, next) => {
    const getId = req.params.id;
    const id = data.people[getId].firstName;
    if (id) {
        res.send(id);
    } else {
        res.status(404).send('Incorrect ID');
    }
});

app.get('/data/people/:id/lastname', (req, res, next) => {
    const getId = req.params.id;
    const id = data.people[getId].lastName;
    if (id) {
        res.send(id);
    } else {
        res.status(404).send('Incorrect ID');
    }
});


app.listen(PORT, () => {
    console.log(`The server has been started on port: ${PORT}`);
});